from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
import math


class PartnarCity(models.Model):
    _name = 'partner.city'
    _rec_name = 'name'
    _description = 'Partner State'

    name = fields.Char(string='Name')
    related_state = fields.Many2one('res.country.state',string='State')


class GoodsMods(models.Model):
    _inherit = 'goods.shipment'
    area_id = fields.Many2one("partner.city", "Area")
    no_orders = fields.Integer("No. Orders", compute="_get_no_orders")

    @api.multi
    @api.onchange("area_id")
    def _get_no_orders(self):
        print(self)
        for item in self:
            if item.area_id:
                orders_count = item.get_orders_by_area(item.area_id.id)
                item.no_orders = len(orders_count)

    def get_orders_by_area(self, area):
        picks = self.env['stock.picking'].search([('partner_id.city', '=', area)])
        print(picks)
        orders_count = []
        for p in picks:
            print ("P:  ", p, p.partner_id.name)
            for i in p.pack_operation_product_ids:
                print("I:   ", i)
                if i.qty_done < i.product_qty  and p not in orders_count:
                    print("CAPTCHA!")
                    orders_count.append(p)
                    continue
        return orders_count

    def action_under_shipment(self):
        super(GoodsMods, self).action_under_shipment()
        print("Getting Shipment .. ")
        self.get_cargo()
        print("Done Shipping!")

    def get_cargo(self):
        pickings = self.get_orders_by_area(self.area_id.id)
        m = (self.area_id.center_latitude or 0,self.area_id.center_longitude or 0)
        res_volume = self.truck_volume
        line_ids = []
        # min_dist = 0.00
        while(len(pickings)):
            nearest_picking = self.find_nearest_picking(m,pickings)
            picking_volume = self.get_picking_volume(nearest_picking)
            if picking_volume <= res_volume:
                line_ids.append(nearest_picking.id)
                res_volume = res_volume - picking_volume
                m = (nearest_picking.partner_id.partner_latitude,nearest_picking.partner_id.partner_longitude)
                pickings.remove(nearest_picking)
        self.line_ids = line_ids        
        return True

    def get_picking_volume(self, picking):
        volume = 0.00
        for line in picking.pack_operation_product_ids:
            volume += line.product_id.volume
        return volume

    def find_nearest_picking(self,m,pickings):
        min_distance = 99999
        cur_nearest_picking = 0
        for p in pickings:
            p_point = (p.partner_id.partner_latitude,p.partner_id.partner_longitude)
            dis = self.calculate_distance(m,p_point)
            if dis < min_distance:
                min_distance = dis
                cur_nearest_picking = p
        return cur_nearest_picking
        
    def calculate_distance(self,origin,destination):
        lat1, lon1 = origin
        lat2, lon2 = destination
        radius = 6371  # km
        dlat = math.radians(lat2 - lat1)
        dlon = math.radians(lon2 - lon1)
        a = (math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dlon / 2) * math.sin(dlon / 2))
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = radius * c
        return d


class PartnerCity(models.Model):
    _inherit = 'partner.city'
    center_longitude = fields.Float("Center Longitude", default=0.00)
    center_latitude = fields.Float("Center Latitude", default=0.00)
    