# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from dateutil.relativedelta import relativedelta as rd
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
from odoo.osv import expression
import re
class InheritSaleOrder(models.Model):
    _inherit = 'sale.order'

    margin_carrier = fields.Integer(string="Margin", required=False,compute='get_margin')
    partner_id = fields.Many2one('res.partner', string='Customer', readonly=True,
                                 states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, required=True,
                                 change_default=True, index=True, track_visibility='always')

    @api.depends('carrier_id')
    def get_margin(self):
        for mar in self:
            if mar.carrier_id:
                mar.margin_carrier = mar.carrier_id.margin

    @api.multi
    def name_get(self):

        def _name_get(p):
            name = p.name
            partner = p.partner_id
            if partner:
                name = '[%s] %s' % (partner.name, name)
            return (p.id, name)

        result = []
        for p in self.sudo():
            result.append(_name_get(p))
        return result

