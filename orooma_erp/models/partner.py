# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api ,_
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT,DEFAULT_SERVER_DATE_FORMAT

LOC_PUR = [('om','Omdurman Market'),
           ('lmk','Local Market Khartoum'),
           ('csb','Central Station Bahri'),
           ('pmo','Peoples Market Omdurman'),
           ('pmk','Peoples Market Khartoum'),
           ('other','Other')]

class ResPartner(models.Model):
    _inherit = 'res.partner'

    name_site = fields.Char(string="Name Of Site", required=True)
    disciption_site = fields.Html(string="Description Site")
    amount_per_trip = fields.Float(string="Amount Per Trip",  required=False, )
    amount_product_per_monthly = fields.Float(string="Amount Product Per Month",  required=False, )
    status = fields.Selection(string="Status Site", selection=[('client', 'Client'),
                                                               ('non_client', 'Non Client'),
                                                               ('churn', 'Churn') ], required=False,default='non_client' )
    purchase_location = fields.Selection(string="Purchase Location",selection=LOC_PUR,default='om')
    status_text = fields.Char(string="Status",readonly=True ,default=_('No Sale Yet'),translate=True)
    sale_order_count_int = fields.Integer()
    sale_order_invoice_amount = fields.Float()
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', required=True)
    city = fields.Many2one('partner.city', required=True)
    state_id = fields.Many2one("res.country.state", required=True,string='State', ondelete='restrict')
    internal_notes = fields.One2many('internal.note', 'partner_id', "Internal Notes")

    @api.multi
    def change_delivery_method_on_partner(self):
        print("Start Changing Status!")
        for partner in self.env['res.partner'].search([('customer','=',True)]):
            partner_state = partner.state_id.id
            partner_city = partner.city.id
            print("State and city: ", partner_state, partner_city)
            if partner_state and partner_city:
                delivary_carrier_obj = self.env['delivery.carrier']
                domain = [('state_id', '=', partner_state),('city', '=', partner_city)]
                delivary_data = delivary_carrier_obj.search(domain,limit=1)
                if delivary_data:
                    partner.property_delivery_carrier_id = delivary_data.id


    @api.onchange('state_id')
    def _onchange_city_id(self):
        if self.country_id:
            return {'domain': {'city': [('related_state', '=', self.state_id.id)]}}
        else:
            return {'domain': {'city': []}}

    @api.model
    def default_get(self, fields):
        res = super(ResPartner, self).default_get(fields)
        sudan_id = self.env['res.country'].search([('phone_code','=','249')],limit=1).id
        if 'country_id' in fields:
            res.update({'country_id': sudan_id})
        return res

    @api.multi
    def name_get(self):

        def _name_get(p):
            name = p.name
            name_site = p.name_site
            phone = p.phone
            if name_site:
                name = '[%s] %s' % (name_site, name)
            if phone:
                name = '[%s] %s' % (phone, name)
            return (p.id, name)

        result = []
        for p in self.sudo():
            result.append(_name_get(p))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search(['|',('phone', operator, name),('name_site', operator, name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

    @api.multi
    def _invoice_total(self):
        account_invoice_report = self.env['account.invoice.report']
        if not self.ids:
            self.total_invoiced = 0.0
            return True

        user_currency_id = self.env.user.company_id.currency_id.id
        all_partners_and_children = {}
        all_partner_ids = []
        for partner in self:
            # price_total is in the company currency
            all_partners_and_children[partner] = self.search([('id', 'child_of', partner.id)]).ids
            all_partner_ids += all_partners_and_children[partner]

        # searching account.invoice.report via the orm is comparatively expensive
        # (generates queries "id in []" forcing to build the full table).
        # In simple cases where all invoices are in the same currency than the user's company
        # access directly these elements

        # generate where clause to include multicompany rules
        where_query = account_invoice_report._where_calc([
            ('partner_id', 'in', all_partner_ids), ('state', 'not in', ['draft', 'cancel']),
            ('company_id', '=', self.env.user.company_id.id),
            ('type', 'in', ('out_invoice', 'out_refund'))
        ])
        account_invoice_report._apply_ir_rules(where_query, 'read')
        from_clause, where_clause, where_clause_params = where_query.get_sql()

        # price_total is in the company currency
        query = """
                      SELECT SUM(price_total) as total, partner_id
                        FROM account_invoice_report account_invoice_report
                       WHERE %s
                       GROUP BY partner_id
                    """ % where_clause
        self.env.cr.execute(query, where_clause_params)
        price_totals = self.env.cr.dictfetchall()
        for partner, child_ids in all_partners_and_children.items():
            partner.total_invoiced = sum(price['total'] for price in price_totals if price['partner_id'] in child_ids)
            partner.sale_order_invoice_amount = partner.total_invoiced




    def _compute_sale_order_count(self):
        sale_data = self.env['sale.order'].read_group(domain=[('partner_id', 'child_of', self.ids)],
                                                      fields=['partner_id'], groupby=['partner_id'])
        # read to keep the child/parent relation while aggregating the read_group result in the loop
        partner_child_ids = self.read(['child_ids'])
        mapped_data = dict([(m['partner_id'][0], m['partner_id_count']) for m in sale_data])
        for partner in self:
            # let's obtain the partner id and all its child ids from the read up there
            partner_ids = filter(lambda r: r['id'] == partner.id, partner_child_ids)[0]
            partner_ids = [partner_ids.get('id')] + partner_ids.get('child_ids')
            # then we can sum for all the partner's child
            partner.sale_order_count = sum(mapped_data.get(child, 0) for child in partner_ids)
            partner.sale_order_count_int = partner.sale_order_count

    @api.multi
    def change_status_partner(self):
        for chg in self.env['res.partner'].search([('customer','=',True)]):
            last_partner_sale = self.env['sale.order'].search([('partner_id', 'child_of', chg.id)],limit=1,order='date_order desc')
            if last_partner_sale:
                today = fields.Date.today()
                order_date = datetime.strptime(last_partner_sale.date_order, DEFAULT_SERVER_DATETIME_FORMAT).date()
                today = datetime.strptime(today, DEFAULT_SERVER_DATE_FORMAT).date()
                days = (today-order_date).days
                status = _('Last Sale Today') if days == 0 else _('Last Sale Before {} Day'.format(days))
                chg.status_text = status
                if days <= 45:
                    chg.status = 'client'
                if days > 45:
                    chg.status = 'churn'



class PartnarCity(models.Model):
    _name = 'partner.city'
    _rec_name = 'name'
    _description = 'Partner State'

    name = fields.Char(string='Name')
    related_state = fields.Many2one('res.country.state',string='State')


class DeliveryCarrier(models.Model):

    _inherit = 'delivery.carrier'

    state_id = fields.Many2one('res.country.state', string='State', required=True,ondelete='restrict')
    city = fields.Many2one('partner.city', required=True)

    @api.onchange('state_id')
    def _onchange_state_id(self):
        sudan_id = self.env['res.country'].search([('phone_code', '=', '249')], limit=1)
        return {'domain': {'city': [('related_state', '=', self.state_id.id)],'state_id': [('country_id', '=', sudan_id.id)]}}


class InternalNote(models.Model):
    _name = 'internal.note'
    name = fields.Datetime("Datetime")
    note = fields.Text("Notes")
    partner_id = fields.Many2one('res.partner')