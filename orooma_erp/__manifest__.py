# -*- coding: utf-8 -*-

{
    'name': 'Orooma ERP',
    'version': '1.0',
    'category': 'Orooma',
    'description': """ Orooma ERP To Managment Transport Action""",
    'license': 'AGPL-3',
    'author': "Sohaib Alabad Alameen Ahmed , Contact: 0924030998 - 0904338004",
    'depends': ['base',
                'sale_automatic_workflow',
                'delivery',
                'orooma_carrier',
                'web_google_maps',
                'sale',
                'purchase',
                'account_accountant',
                'l10n_generic_coa',
                'base_geolocalize',
                'sale_delivery_rate',
                'stock_picking_delivery_rate',
                'backend_theme_v10',
                'web_responsive',
                'odoo_web_login',
                'web_hide_db_manager_link',],
    'data': [
        'views/partner_view.xml',
        'views/sale_order.xml',
        'data/state_partner_data.xml',
    ],
    'installable': True,
}
