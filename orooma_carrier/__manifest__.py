# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Delivery Carrier',
    'version': '1.0',
    'category': 'Stock',
    'description': """
Allows you to add delivery methods in sale orders and picking.
==============================================================

You can define your own carrier for prices. When creating
invoices from picking, the system is able to add and compute the shipping line.
""",
    'depends': ['base', 'delivery'],
    'data': [
        'security/shipment_security.xml',
        'security/ir.model.access.csv',
        'views/res_company_view.xml',
        'views/goods_shipment_view.xml',
        'views/orooma_carrier_view.xml',
        'views/goods_shipment.xml',
        'data/shipment_sequence.xml',
        'data/shipment_cron.xml',
        'wizard/cancel_wizard_view.xml',
        'wizard/partial_deliver_reason_wizard_view.xml',
        'report/shipment_report.xml',
    ],

'qweb': [
            "static/src/xml/good_shipment.xml",
        ],

    'installable': True,
}
