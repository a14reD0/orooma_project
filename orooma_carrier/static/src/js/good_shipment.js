odoo.define('goods.shipment', function (require) {
"use strict";

var core = require('web.core');
var form_common = require('web.form_common');
var formats = require('web.formats');
var Model = require('web.Model');

var QWeb = core.qweb;
var _t = core._t;

var ShowPartnerLineWidget = form_common.AbstractField.extend({
    render_value: function() {
//    console.log('tttttttttttttttttttttttttttttttyyy');
        var self = this;
        var info = JSON.parse(this.get('value'));
//        console.log('tttttttttttttttttttttttttttttttzzz',info);
        if (info !== false) {
            this.$el.html(QWeb.render('ShowPartnerInfo', {
                'lines': info.content, 
                'partner': info.outstanding,
                'title': info.title,
            }));
        }
        else {
            this.$el.html('');
        }
    },
});

core.form_widget_registry.add('partner', ShowPartnerLineWidget);

});
