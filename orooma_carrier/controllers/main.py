# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json
from odoo import http, _, fields
from odoo.http import request

class ShipmentController(http.Controller):


    @http.route('/customer/notes', type='json', auth='public')
    def get_all_internal_notes(self,*args, **kwargs):
        customer_id = request.params['customer_id']
        return request.env['res.partner'].sudo().search([('id', '=', customer_id)]).internal_notes.read(['name', 'note'])
    
    @http.route('/vendors/list', type='json', auth='public')
    def get_all_vendors(self,*args, **kwargs):
        return request.env['res.partner'].sudo().search([('supplier', '=', 1)]).read(['name', 'id', 'street', 'street2', 'state_id', 'country_id', 'phone', 'email'])
    
    @http.route('/products/list', type='json', auth='public')
    def get_all_products(self,*args, **kwargs):
        return request.env['product.template'].sudo().search([]).read(['name', 'id', 'image_medium', 'list_price'])
    
    @http.route('/vendors/new_vendor', type='json', auth='public')
    def get_all_vendors(self,*args, **kwargs):
        new_vendor = request.env['res.partner'].sudo().create({
            'name': request.params['vendor_name'],
            'supplier': 1,
            'phone': request.params['phone'],
            'name_site': request.params['name_site'],
            'city': request.params['city_id'],
            'state_id': request.params['state_id'],
            'country_id': request.params['country_id'],
        })
        return new_vendor.read(['name', 'id', 'street', 'street2', 'state_id', 'country_id', 'phone', 'email'])
    
    @http.route('/customer/new_note', type='json', auth='public')
    def add_internal_note(self,*args, **kwargs):
        customer_id = request.params['customer_id']
        request.env['internal.note'].sudo().create({
            'partner_id': customer_id,
            'name': fields.Datetime.now(),
            'note': request.params['internal_note'],
        })
        return True
        
    @http.route('/order/delivery_status/update', type='json', method='POST', auth='public')
    def update_delivery_status(self, *args, **kwargs):
        request.env['goods.shipment'].sudo().search([
            ('shipment_id', '=', request.params['shipment_id']),
        ]).state = request.params['new_state']
        return True

    @http.route(['/shipment/orders', '/shipment/orders/<truck>'], type='http', auth="public", website=True)
    def my_projects(self, truck=None, **kw):
        under_shipment_ids = request.env['goods.shipment'].search([('truck_id','=',int(truck)),('state','in', ['under_shipment'])])
        data = {}
        partner_list_ids = []
        if len(under_shipment_ids) == 0:
            data['data'] = 'There Is No Shipment Data Found For Truck Requested'
        else:
            for sh in under_shipment_ids:
                partner_list_ids = sh.line_ids.mapped('partner_id').ids

            partner_shipment_list = []
            for partner in partner_list_ids:
                partner_data_dict = {}
                partner_id = request.env['res.partner'].browse(partner)
                for sh in under_shipment_ids:
                    partner_line_ids = sh.line_ids.filtered(lambda p:p.partner_id.id == partner)
                    partner_data_dict['latitude'] = partner_id.partner_latitude
                    partner_data_dict['shipment'] = sh.id
                    partner_data_dict['longitude'] = partner_id.partner_longitude
                    partner_data_dict['partner_id'] = partner_id.id
                    partner_data_dict['partner_name'] = partner_id.name
                    partner_data_dict['site_name'] = partner_id.name_site
                    partner_data_dict['area'] = partner_id.city.name
                    partner_data_dict['address'] = partner_id.state_id.name
                    partner_data_dict['phone'] = partner_id.phone
                    partner_data_dict['mobile'] = partner_id.mobile

                    pick_id = self.get_picking_from_operation_line_for_partner(partner_line_ids)
                    partner_data_dict['picking_id'] = pick_id.id
                    partner_data_dict['picking_name'] = pick_id.name

                    product_list = []
                    for partner_line_id in partner_line_ids:
                        prodcut_dict = {}
                        prodcut_dict['product_id'] = partner_line_id.product_id.id
                        prodcut_dict['product_name'] = partner_line_id.product_id.name
                        prodcut_dict['product_price'] = partner_line_id.product_id.lst_price
                        prodcut_dict['qty'] = partner_line_id.product_qty
                        prodcut_dict['id'] = partner_line_id.id

                        product_list.append(prodcut_dict)

                    partner_data_dict['products'] = product_list
                    partner_shipment_list.append(partner_data_dict)
            data['data'] = partner_shipment_list
        shipment_json_data = json.dumps(data)
        return shipment_json_data

    def get_picking_from_operation_line_for_partner(self, lines):
        if lines:
            picking_id = lines[0].picking_id
            print "picking id",picking_id
            for line in lines:
                if line.picking_id != picking_id:
                    return ''
            return picking_id

