from odoo import models, fields, api

class ShipmentRepert(models.AbstractModel):
    _name = 'report.orooma_carrier.report_good_shipment_template'

    def get_partners(self, shipment):
        picking = shipment.line_ids.mapped("picking_id")
        partner = set(picking.mapped('partner_id').ids)
        partner_ids = self.env['res.partner'].browse(partner)
        return partner_ids

    def get_goods_in_shipment_for_partner(self, shipment, partner):
        return shipment.line_ids.filtered(lambda i:i.picking_id.partner_id.id == partner.id)

    @api.model
    def render_html(self, docids, data=None):
        shipment = self.env['goods.shipment'].browse(docids)
        for i in shipment:
            self.get_partners(i)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'goods.shipment',
            'docs': shipment,
            'data': data,
            'partner':self.get_partners,
            'goods':self.get_goods_in_shipment_for_partner
            }
        return self.env['report'].render('orooma_carrier.report_good_shipment_template', docargs)