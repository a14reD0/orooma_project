from odoo import models, fields, api

class PartialReasonWizard(models.TransientModel):

    _name = "partial.reason.wizard"

    reason = fields.Char(string='Reason', required=True)

    @api.multi
    def partail_reason(self):
        self.ensure_one()
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        sh = self.env['goods.shipment'].browse(active_ids)
        sh.partial_reason(self.reason)
        return {'type': 'ir.actions.act_window_close'}