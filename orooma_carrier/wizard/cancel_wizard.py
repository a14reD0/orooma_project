# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class CancelWizard(models.TransientModel):

    _name = "deliverd.cancel.wizard"

    reason = fields.Char(string='Reason', required=True)

    @api.multi
    def deliverd_cancel_reason(self):
        self.ensure_one()
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        sh = self.env['goods.shipment'].browse(active_ids)
        sh.button_cancel(self.reason)
        return {'type': 'ir.actions.act_window_close'}
