from odoo import api, fields, models

class Company(models.Model):
    _inherit = 'res.company'

    partner_id = fields.Many2one('res.partner')
    distancematrix_key = fields.Char(string="Google Matrix Key", default="AIzaSyDIj6AuzrorbzMhbXbh16g6qXNcQmdUZn8")
    sale_shipment_after_no = fields.Integer("Shipment After No Of Sale", default=-1)