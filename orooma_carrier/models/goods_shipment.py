from datetime import date
import json
import requests

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

class GoodsShipment(models.Model):
    _name = "goods.shipment"

    name = fields.Char(string="Shipment No", default="New", readonly=1)
    state = fields.Selection([('draft', 'Draft'),
                              ('under_shipment', 'Under Shipment'),
                              ('in_way', 'In Way'),
                              ('partially_delivered', 'Partially Delivered'),
                              ('delivered', 'Delivered'),
                              ('not_delivered','Not Delivered')], default="draft")
    truck_id = fields.Many2one("orooma.delivery.carrier", string="Truck")
    date = fields.Date(string="Shipment Date", default=date.today())
    driver_phone1 = fields.Char(related="truck_id.driver_phone1", string="Phone")
    truck_volume = fields.Float(related="truck_id.max_volume", string="Truck Volume")
    line_ids = fields.Many2many("stock.pack.operation")
    customer_info_widget = fields.Text(compute="_show_customer_info_widget")
    not_deliverd_ids = fields.One2many("goods.not_deliverd", 'shipment_id')
    shipment_id = fields.Many2one("goods.shipment")
    is_change1 = fields.Boolean(compute="_change_current_status")
    is_change2 = fields.Boolean(compute="_change_current_status")
    not_deliverd_reason = fields.Char()

    def _get_no_orders(self):
        return True

    def geo_find_distance(self, from_lat, from_long, to_lat, to_long):
        google_map_matrix_key = self.env.user.company_id.distancematrix_key
        if google_map_matrix_key in [False, None]:
            raise ValidationError('Please Set google map key in your company config')
        PARAMS = {'origins': str(from_lat)+','+str(from_long),
                  'destinations': str(to_lat)+','+str(to_long),
                  'key': google_map_matrix_key
                  }
        url = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
        try:
            r = requests.get(url=url, params=PARAMS)
            data = r.json()
        except Exception as e:
            raise UserError(_(
                'Cannot contact geolocation servers. Please make sure that your Internet connection is up and running (%s).') % e)

        if data['status'] != 'OK':
            return None
        if data['rows'][0]['elements'][0]['status'] != 'ZERO_RESULTS':
            value = data['rows'][0]['elements'][0]['distance']['value']
            return value

    @api.multi
    def get_distance_vals(self):
        base_lat , base_long = self.get_base_partner_lag_long()
        data_list = []
        picking_ids = self.env['stock.picking'].search([('state', 'not in', ['done', 'cancel'])])
        for p in picking_ids:
            data_dict = {}
            p_lat = p.partner_id.partner_latitude
            p_long = p.partner_id.partner_longitude
            distance_in_value = self.geo_find_distance(base_lat, base_long, p_lat, p_long)
            if distance_in_value != None:
                data_dict['distace'] = distance_in_value
                data_dict['picking_id'] = p.id
                data_list.append(data_dict)
        data_sorted = self.sort_list_of_dict(data_list, 'distace')
        return data_sorted

    def get_base_partner_lag_long(self):
        partner_id = self.env.user.company_id.partner_id
        if partner_id in [False, None]:
            raise ValidationError('Please Set base partner in your company config')
        else:
            return partner_id.partner_latitude, partner_id.partner_longitude

    def sort_list_of_dict(self, lod, by):
        data_sorted = sorted(lod, key=lambda item: item[by])
        return data_sorted

    def get_available_carrier_with_sorted_proirity(self):
        carrier_ids = self.env['orooma.delivery.carrier'].search([('state','=','available')])
        carr_list = []
        for c in carrier_ids:
            carr_dict = {}
            carr_dict['id'] = c.id
            carr_dict['priority'] = c.priority
            carr_list.append(carr_dict)
        sorted_carr_data = self.sort_list_of_dict(carr_list, 'priority')
        sorted_carr_data.reverse()
        return sorted_carr_data

    @api.multi
    def create_auto_shipment(self):
        print("Auto-shipment begins!")
        shipment_after_no = self.env.user.company_id.sale_shipment_after_no
        no_of_picking_under_shipment = self.env['stock.picking'].search_count([('state', 'not in', ['done', 'cancel'])])
        print("No of Picking: ", no_of_picking_under_shipment)
        if shipment_after_no == -1 or shipment_after_no <= no_of_picking_under_shipment:
            shipment_obj = self.env['goods.shipment']
            carrier_obj = self.env['orooma.delivery.carrier']
            available_carr = self.get_available_carrier_with_sorted_proirity()
            data_list = self.get_all_pack_operation_product_ids()
            print("Available cars: ", available_carr)
            print("Data List: ", data_list)
            for carr in available_carr:
                pack_list = []
                list_to_remove = []
                truck_id = carrier_obj.browse(carr['id'])
                for dl in data_list:
                    pack_list.append(dl)
                    total_volume = self.get_shipment_volume(pack_list)
                    if total_volume > truck_id.max_volume:
                        pack_list.pop()
                        break
                    list_to_remove.append(dl)
                if len(pack_list) > 0:
                    print "xxxxxxxxxxxxxxxxxxxxxxxxx"
                    shipment_id = shipment_obj.create({'truck_id': truck_id.id})
                    shipment_id.action_under_shipment()
                    shipment_id.write({'line_ids': [(6,0, pack_list)]})

                # remove inserted pack
                for ltr in list_to_remove:
                    data_list.remove(ltr)
                if len(data_list) == 0:
                    break

    def get_shipment_volume(self, list):
        pack_obj = self.env['stock.pack.operation']
        pack_ids = pack_obj.browse(list)
        total_volume = sum((pack.product_id.volume *  pack.product_qty) for pack in pack_ids)
        return total_volume

    def get_all_pack_operation_product_ids(self):
        picking_obj = self.env['stock.picking']
        distance_vals = self.get_distance_vals()
        pack_list = []
        for d  in distance_vals:
            picking_id = picking_obj.browse(d ['picking_id'])
            pack_list.extend(picking_id.pack_operation_product_ids.ids)
        return pack_list

    @api.multi
    def call_cancel_wizard(self):
        ctx = self._context.copy()
        action_rec = self.env['ir.model.data'].xmlid_to_object('orooma_carrier.deliverd_cancel_wizard_action')
        if action_rec:
            action = action_rec.read([])[0]
            action['context'] = ctx
            return action

    @api.multi
    def call_partial_deliverd_wizard(self):
        ctx = self._context.copy()
        action_rec = self.env['ir.model.data'].xmlid_to_object('orooma_carrier.deliverd_partial_wizard_action')
        if action_rec:
            action = action_rec.read([])[0]
            action['context'] = ctx
            return action

    @api.model
    def button_cancel(self, reason):
        self.not_deliverd_reason = reason
        self.state = 'not_delivered'
        self.truck_id.state = 'available'

    @api.model
    def partial_reason(self, reason):
        self.not_deliverd_reason = reason
        self.state = 'partially_delivered'

    @api.multi
    def action_under_shipment(self):
        self.shipment_id = self.id
        self.state = 'under_shipment'
        self.truck_id.state = 'not_available'

    @api.multi
    def action_in_way(self):
        active_id = self._context.get('active_id')
        if self.line_ids:
            deliverd_obj = self.env['goods.deliverd']
            for i in self.line_ids:
                # set shiped qty
                i.shipment_id = active_id
                deliverd_obj.create({'picking_id': i.picking_id.id,
                                     'product_id': i.product_id.id,
                                     'shiped_qty': i.not_shiped_qty})
                # set deliverd qty
                sh_id = self.get_not_shiped_lines(i.picking_id.id, i.product_id.id, self.shipment_id.id)
                if sh_id.id:
                    sh_id.write({'deliverd_qty': i.not_shiped_qty})
        self.state = 'in_way'

    @api.multi
    def action_deliverd(self):
        if self.line_ids and self.line_ids.ids:
            line_filtered_ids = self.line_ids.filtered(lambda l: l.not_shiped_qty != l.deliverd_qty)
            if len(line_filtered_ids.ids) == 0:
                self.write({'state': 'delivered'})
                self.truck_id.state = 'available'
            else:
                return self.call_partial_deliverd_wizard()

    @api.multi
    def action_not_deliverd(self):
        return self.call_cancel_wizard()

    @api.model
    def create(self, vals):
        if 'name' not in vals or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('ship.sequence') or _('New')
        res = super(GoodsShipment, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        rec = self
        res = super(GoodsShipment, self).write(vals)
        self.create_not_shiped_lines_qty(vals)
        return res

    @api.one
    def _show_customer_info_widget(self):
        self.customer_info_widget = json.dumps(False)
        partner_list = set([l.picking_id.partner_id.id for l in self.line_ids])
        partner_ids = self.env['res.partner'].browse(partner_list)
        info = {'title': _('Show Partners'), 'partner': False, 'content': []}
        if partner_ids.ids:
            for partner in partner_ids:
                total_volume, total_shiped_qty = self.get_partner_info(partner.id)
                info['content'].append({
                    'name': partner.name,
                    'site_name': partner.name_site,
                    'total_volume': total_volume,
                    'total_shiped_qty': total_shiped_qty,
                })
        self.customer_info_widget = json.dumps(info)


    def get_partner_info(self, partner_id):
        rec = self
        total_volume = total_shiped_qty = 0.00
        if rec.line_ids:
            line_ids = rec.line_ids.filtered(lambda l:l.picking_id.partner_id.id == partner_id)
            total_volume = sum(line_ids.mapped('total_volume'))
            total_shiped_qty = sum(line_ids.mapped('shiped_qty'))
        return total_volume, total_shiped_qty

    def get_not_shiped_lines(self, pick_id, product_id, shipment_id):
        not_deliverd_obj = self.env['goods.not_deliverd']
        if shipment_id:
            shipment_id = not_deliverd_obj.search([('picking_id', '=', pick_id),
                                          ('product_id', '=', product_id),
                                          ('shipment_id', '=', shipment_id)])

        return shipment_id


    def _check_all_item_in_picking_is_shiped(self):
        pick_ids = set([i.picking_id.id for i in self.line_ids])
        pick_ids = self.env['stock.picking'].browse(pick_ids)
        for pick in pick_ids:
            pick_filtered_ids = pick.pack_operation_product_ids.filtered(lambda l: l.product_qty != l.shiped_qty)
            if len(pick_filtered_ids.ids) == 0:
                if pick.state != 'done':
                    for p in pick.pack_operation_product_ids:
                        p.write({'qty_done':i.product_qty})
                        # self.do_picking_transfar(pick)
                        pick.write({'state': 'done'})

    # @api.multi
    # def do_picking_transfar(self,pick_id):
    #     if pick_id:
    #         stock_immediate_transfer = pick_id.do_new_transfer()
    #         apply = self.env[stock_immediate_transfer['res_model']].browse(stock_immediate_transfer['res_id'])
    #         apply.process()

    @api.depends('truck_id','date', 'shipment_id', 'driver_phone1', 'line_ids', 'state')
    def _change_current_status(self):
        self._check_all_item_in_picking_is_shiped()
        for i in self.line_ids:

            sh_id = self.get_not_shiped_lines(i.picking_id.id, i.product_id.id, self.shipment_id.id)
            if sh_id:
                if sh_id.shipment_id.id == self.shipment_id.id:
                    i.write({'not_shiped_qty': sh_id.not_shiped_qty,
                             'deliverd_qty': sh_id.deliverd_qty,
                             'shipment_id': sh_id.shipment_id.id})
            else:
                i.write({'not_shiped_qty': i.product_qty - i.shiped_qty,
                         'deliverd_qty': 0.0,
                         'shipment_id': sh_id.shipment_id.id})


        self._domain_for_product_in_picking()
        if self.is_change1 == False:
            self.is_change2 = True
        if self.is_change2 == True:
            self.is_change1 = False

        if self.is_change1 == True:
            self.is_change2 = False
        if self.is_change2 == False:
            self.is_change1 = True

    @api.onchange('line_ids', 'state')
    def _domain_for_product_in_picking(self):
        self.is_change1 = True
        stock_pack_operation_ids = self.env['stock.pack.operation'].search([])
        stock_pack_operation_ids = stock_pack_operation_ids.filtered(lambda s: (s.picking_id.state not in ['done', 'cancel']) and (s.product_qty != s.shiped_qty))
        domain = {'line_ids': [('id', 'in', stock_pack_operation_ids.ids)]}
        return {'domain': domain}


    def create_not_shiped_lines_qty(self, lines):
        not_deliverd_obj = self.env['goods.not_deliverd']
        for i in self.line_ids:
            not_deliverd_ids = self.get_not_deliverd_line(i.picking_id.id, i.product_id.id, self.id)
            if not not_deliverd_ids.ids:
                not_deliverd_obj.create({'picking_id': i.picking_id.id,
                                         'product_id': i.product_id.id,
                                         'not_shiped_qty': i.not_shiped_qty,
                                         'deliverd_qty': i.deliverd_qty,
                                         'shipment_id': self.shipment_id.id})
            else:
                if lines:
                    if 'line_ids' in lines:
                        if len(lines['line_ids']) == 1:
                            pack_id = self.get_stock_pack_operation(lines['line_ids'][0][1])
                            not_deliverd_ids = self.get_not_deliverd_line(pack_id.picking_id.id, pack_id.product_id.id, self.id)
                            if 'not_shiped_qty' in lines['line_ids'][0][2]:
                                not_deliverd_ids.not_shiped_qty = lines['line_ids'][0][2]['not_shiped_qty']
                            if 'deliverd_qty' in lines['line_ids'][0][2]:
                                not_deliverd_ids.deliverd_qty = lines['line_ids'][0][2]['deliverd_qty']
                        elif len(lines['line_ids']) > 1:
                            for i in lines['line_ids']:
                                pack_id = self.get_stock_pack_operation(i[1])
                                not_deliverd_ids = self.get_not_deliverd_line(pack_id.picking_id.id,
                                                                              pack_id.product_id.id, self.id)
                                if 'not_shiped_qty' in i[2]:
                                    not_deliverd_ids.not_shiped_qty = i[2]['not_shiped_qty']
                                if 'deliverd_qty' in i[2]:
                                    not_deliverd_ids.deliverd_qty = i[2]['deliverd_qty']



    def get_not_deliverd_line(self, picking_id, product_id, shipment_id):
        not_deliverd_obj = self.env['goods.not_deliverd']
        not_deliverd_ids = not_deliverd_obj.search([('picking_id', '=', picking_id),
                                                    ('product_id', '=', product_id),
                                                    ('shipment_id', '=',shipment_id)])

        return  not_deliverd_ids

    def get_stock_pack_operation(self, pack_id):
        pack_id = self.env['stock.pack.operation'].browse(pack_id)
        return pack_id

    # @api.constrains('line_ids')
    # def _check_volume(self):
    #     rec = self
    #     total_volume = sum((l.product_volume * l.shiped_qty) for l in rec.line_ids)
    #     print "total volume------------------",total_volume
    #     print "truck volume------------------",rec.truck_volume
    #     if total_volume > rec.truck_volume:
    #         raise ValidationError(_('Shippment Volume is greater than truck volume, Max weight for truck %s is %s')%(rec.truck_id.name, rec.truck_volume))


class StockPackOperation(models.Model):
    _inherit = "stock.pack.operation"

    product_volume = fields.Float(related="product_id.volume")
    not_shiped_qty = fields.Float(string="To Do Ship")
    shiped_qty = fields.Float(string="Done Ship", compute="_compute_done_and_loss_qty")
    deliverd_qty = fields.Float(string="Deliverd Qty")
    loss_qty = fields.Float(string="Loss Quantity", compute="_compute_done_and_loss_qty")
    total_volume = fields.Float(string="Total Volume", compute="_total_volume")
    shipment_id = fields.Many2one("goods.shipment")
    is_to_shiped_editable = fields.Boolean(compute="_check_current_state")
    is_deliverd_editable = fields.Boolean(compute="_check_current_state")
    partner_id = fields.Many2one(related="picking_id.partner_id")

    def _check_current_state(self):
        for i in self:
            i.is_deliverd_editable = False
            i.is_to_shiped_editable = False
            if i.shipment_id and i.shipment_id.id:
                if i.shipment_id.state == 'under_shipment':
                    i.is_to_shiped_editable = True
                if i.shipment_id.state in ['in_way']:
                    i.is_deliverd_editable = True

    def _compute_done_and_loss_qty(self):
        for i in self:
            i.shiped_qty = self.get_shiped_lines_qty(i.picking_id.id, i.product_id.id)
            i.loss_qty = i.not_shiped_qty - i.deliverd_qty

    def get_shiped_lines_qty(self, pick_id, product_id):
        deliverd_obj = self.env['goods.deliverd']
        sh_qty = sum(deliverd_obj.search([('picking_id.id', '=', pick_id),
                                          ('product_id.id', '=', product_id)]).mapped('shiped_qty'))
        return sh_qty

    def get_not_shiped_lines_qty(self, pick_id, product_id, shipment_id):
        not_deliverd_obj = self.env['goods.not_deliverd']
        if shipment_id:
            sh_qty = not_deliverd_obj.search([('picking_id', '=', pick_id),
                                          ('product_id', '=', product_id),
                                          ('shipment_id', '=', shipment_id)]).mapped('not_shiped_qty')
            if sh_qty:
                return sh_qty[0]
        return 0.00

    def get_not_shiped_lines(self, pick_id, product_id, shipment_id):
        not_deliverd_obj = self.env['goods.not_deliverd']
        if shipment_id:
            shipment = not_deliverd_obj.search([('picking_id', '=', pick_id),
                                          ('product_id', '=', product_id),
                                          ('shipment_id', '=', shipment_id)]).shipment_id
        return shipment.id

    def _domain_for_product_in_picking(self):
        stock_pack_operation_ids = self.env['stock.pack.operation'].search([])
        stock_pack_operation_ids = stock_pack_operation_ids.filtered(
            lambda s: s.picking_id.state not in ['done', 'cancel'])
        domain = {'ids': [('id', 'in', stock_pack_operation_ids.ids)]}
        return {'domain': domain}

    def _total_volume(self):
        for i in self:
            i.total_volume = i.product_volume * i.not_shiped_qty

    # @api.constrains('deliverd_qty', 'shiped_qty', 'loss_qty')
    def _check_qty(self):
        for i in self:
            print "ship",i.not_shiped_qty + i.shiped_qty
            if i.shiped_qty > i.product_qty:
                raise ValidationError(_("Shiped quantity cannot be greater than requested quantity"))
            if i.deliverd_qty > i.not_shiped_qty:
                raise ValidationError(_("Deliverd Quantity cannot be greater than shipped Quantity"))
            if i.deliverd_qty < 0:
                raise ValidationError(_("The Quantity of Delivary must be greater than zero"))
            if i.not_shiped_qty < 0:
                raise ValidationError(_("The Quantity of shipment must be greater than zero"))


class GoodsDeliverd(models.Model):
    _name = "goods.deliverd"

    picking_id = fields.Many2one("stock.picking")
    product_id = fields.Many2one("product.product")
    shiped_qty = fields.Float()


class GoodsNotDeliverd(models.Model):
    _name = "goods.not_deliverd"

    picking_id = fields.Many2one("stock.picking")
    product_id = fields.Many2one("product.product")
    not_shiped_qty = fields.Float()
    deliverd_qty = fields.Float()
    shipment_id = fields.Many2one("goods.shipment")

