import json
import urllib2

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

def geo_find(addr):
    if not addr:
        return None
    url = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='
    url += urllib2.quote(addr.encode('utf8'))

    try:
        result = json.load(urllib2.urlopen(url))
    except Exception as e:
        raise UserError(_('Cannot contact geolocation servers. Please make sure that your Internet connection is up and running (%s).') % e)

    if result['status'] != 'OK':
        return None

    try:
        geo = result['results'][0]['geometry']['location']
        return float(geo['lat']), float(geo['lng'])
    except (KeyError, ValueError):
        return None

def geo_query_address(street=None, zip=None, city=None, state=None, country=None):
    if country and ',' in country and (country.endswith(' of') or country.endswith(' of the')):
        # put country qualifier in front, otherwise GMap gives wrong results,
        # e.g. 'Congo, Democratic Republic of the' => 'Democratic Republic of the Congo'
        country = '{1} {0}'.format(*country.split(',', 1))
    return tools.ustr(', '.join(filter(None, [street,
                                              ("%s %s" % (zip or '', city or '')).strip(),
                                              state,
                                              country])))

class OroomaCarrier(models.Model):
    _name = "orooma.delivery.carrier"

    name = fields.Char("Carrier Name")
    # driver_id = fields.Many2one("res.partner", string="Driver Name")
    driver_id = fields.Many2one("res.users", string="Driver Name")
    driver_phone1 = fields.Char(related="driver_id.phone")
    driver_phone2 = fields.Char(related="driver_id.mobile")
    license_plate = fields.Char(string="License Plate")

    max_weight = fields.Float(string="Max Weight")
    max_volume = fields.Float(string="Max Volume")

    street = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')

    carrier_latitude = fields.Float(string='Geo Latitude', digits=(16, 5))
    carrier_longitude = fields.Float(string='Geo Longitude', digits=(16, 5))
    date_localization = fields.Date(string='Geolocation Date')

    state = fields.Selection([('available', 'Available'),('not_available', 'Not Available')], default='available')
    priority = fields.Selection([('1','1'),('2','2'),('3','3'),('4','4'),('5','5')], default='1')

    @api.multi
    def geo_localize(self):
        # We need country names in English below
        for partner in self.with_context(lang='en_US'):
            result = geo_find(geo_query_address(street=partner.street,
                                                zip=partner.zip,
                                                city=partner.city,
                                                state=partner.state_id.name,
                                                country=partner.country_id.name))
            if result is None:
                result = geo_find(geo_query_address(
                    city=partner.city,
                    state=partner.state_id.name,
                    country=partner.country_id.name
                ))

            if result:
                partner.write({
                    'carrier_latitude': result[0],
                    'carrier_longitude': result[1],
                    'date_localization': fields.Date.context_today(partner)
                })
        return True


class UserMods(models.Model):
    _inherit = 'res.users'
    truck_id = fields.Many2one('orooma.delivery.carrier', "Carrier")


class ProductTemplateMods(models.Model):
    _inherit= 'product.template'
    pricing_method = fields.Selection([('manual', "Manual"), ('percentage', "Percentage")], default='manual', string="Pricing Method")
    price_percentage = fields.Float("Price_percentage", default=0)
    
    @api.onchange('pricing_method','percentage','standard_price')
    def set_price(self):
        if self.pricing_method == 'percentage':
            self.list_price = self.standard_price + self.standard_price * self.price_percentage / 100
